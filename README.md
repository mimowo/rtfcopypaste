# README #

Netbeans plugin to copy/paste editor content with formatting to RTF format. This allows to to copy code with highlighted syntax to presentations.

# Usage

1. Select code,
1. Click right mouse button and select "Copy as RTF format" to copy, selected content to system clipboard,
1. Paste clipboard content to some RTF editor like OpenOffice or MSWord (special paste).

# Additional options

1. Go to Tools - Options
1. Select Copy/Paste tab to customize plugin options.

# Development

This project is developed as open source, here: https://bitbucket.org/mimowo/rtfcopypaste. You are welcome to contribute. Special thanks for contributing: 

* akks - support for utf8

# Older versions

6.5: [http://plugins.netbeans.org/plugin/16957]

6.5.1: [http://plugins.netbeans.org/plugin/16957/]

6.7.1: [http://plugins.netbeans.org/plugin/42218/]

6.8: [http://plugins.netbeans.org/plugin/22537/]

6.9.1: [http://plugins.netbeans.org/plugin/32264/]

7.0: [http://plugins.netbeans.org/plugin/39279/]

7.0.1: [http://plugins.netbeans.org/plugin/39977/]

7.4: [http://plugins.netbeans.org/plugin/51461/]

8.0: [http://plugins.netbeans.org/plugin/54459/]

8.0.1: [http://plugins.netbeans.org/plugin/56173/]

8.0.2: [http://plugins.netbeans.org/plugin/56968/]

8.1: [http://plugins.netbeans.org/plugin/61409/]