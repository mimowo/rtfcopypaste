package org.netbeans.modules.rtfcopypaste.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.util.Exceptions;

public class HighlightsRtfConverter extends RtfConverter {

    private static final Logger LOGGER = Logger.getLogger(HighlightsRtfConverter.class.getName());

    @Override
    protected List<TokenWithStyle> createTokensWithStyle(JEditorPane pane, FontColorSettings fcs) {
        List<TokenWithStyle> tokensWithStyle = new ArrayList<TokenWithStyle>();

        int start = pane.getSelectionStart();
        int end = pane.getSelectionEnd();

        if (start == end) {
            start = 1;
            end = pane.getText().length();
        }

        HighlightsContainer bh = HighlightingManager.getInstance(pane).getBottomHighlights();
        HighlightsSequence bhs = bh.getHighlights(start, end);
        int token_end_prev = -2;

        while (bhs.moveNext()) {
            int token_start = bhs.getStartOffset();
            int token_end = bhs.getEndOffset();

            if (token_start > token_end_prev && token_end_prev >= 0) {
                String missTokenText = getTextFromPane(pane, token_end_prev, token_start);
                TokenWithStyle missToken = new TokenWithStyle(missTokenText);
                tokensWithStyle.add(missToken);
            }
            String tokenText = getTextFromPane(pane, token_start, token_end);
            TokenWithStyle token = new TokenWithStyle(tokenText);
            updateTokenStyle(token, bhs.getAttributes());
            tokensWithStyle.add(token);
            token_end_prev = token_end;
        }
        for(TokenWithStyle token: tokensWithStyle) {
            LOGGER.info("token high: " + token.getToken());
        }
        return tokensWithStyle;
    }

    private String getTextFromPane(JEditorPane pane, int token_start, int token_end) {
        try {
            String token = pane.getText(token_start, token_end - token_start);
            return token;
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return "";
    }
}
