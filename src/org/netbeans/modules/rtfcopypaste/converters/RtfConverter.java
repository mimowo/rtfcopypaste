package org.netbeans.modules.rtfcopypaste.converters;

import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.Coloring;
import org.netbeans.modules.rtfcopypaste.utils.ConverterSettings;

public abstract class RtfConverter {

    private static final Logger LOGGER = Logger.getLogger(RtfConverter.class.getName());
    private static final String eol = System.getProperty("line.separator");

    public String convertContentToRtf(JEditorPane pane) {
        final FontColorSettings fcs = MimeLookup.getLookup(
                MimePath.get(pane.getContentType())).lookup(FontColorSettings.class);

        List<TokenWithStyle> tokensWithStyle = createTokensWithStyle(pane, fcs);
        Color defColor = (Color) fcs.getTokenFontColors("default").getAttribute(StyleConstants.Foreground);
        return buildRtfFromTokens(defColor, tokensWithStyle);
    }

    protected abstract List<TokenWithStyle> createTokensWithStyle(JEditorPane pane,
            FontColorSettings fcs);

    protected void updateTokenStyle(TokenWithStyle tokenWithStyle, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        final Color fg = (Color) attrs.getAttribute(StyleConstants.Foreground);
        tokenWithStyle.setFg(fg);
        final Font font = Coloring.fromAttributeSet(attrs).getFont();
        tokenWithStyle.setFont(font);
    }

    private ColorTable createColorTable(Color defColor, List<TokenWithStyle> tokensWithStyle) {
        ColorTable colorTable = new ColorTable(defColor);

        for (TokenWithStyle tokenWithStyle : tokensWithStyle) {
            Color fg = tokenWithStyle.getFg();
            if (fg != null) {
                colorTable.addColor(fg);
            }
        }
        return colorTable;
    }

    private String buildRtf(String fonttable, String colortable, String content) {
        Integer fontsize = ConverterSettings.getDefault().getSelectedFontSize() * 2;
        return buildRtf(fonttable, colortable, content, fontsize);
    }

    private String buildRtf(String fonttable, String colortable, String content, Integer fontsize) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\\rtf1\\ansi\\deff0 ").append(eol);
        sb.append(fonttable).append(eol);
        sb.append(colortable).append(eol);
        sb.append("\\f0\\fs").append(fontsize).append(eol);
        sb.append(content).append("}").append(eol);
        return sb.toString();
    }

    private String fontTableToRtf() {
        return "{\\fonttbl {\\f0 Courier New;}}";
    }

    private String colorToRtf(Color color) {
        return "\\red" + color.getRed() + "\\green" + color.getGreen() + "\\blue"
                + color.getBlue();
    }

    private String tokenToRtf(final String oldcontent) {
        String content = escapeUtfCharsToRtf(oldcontent);
        StringBuilder sb = new StringBuilder(content.length());

        for (int i = 0; i < content.length(); i++) {
            sb.append(escapeCharToRtf(oldcontent.charAt(i)));
        }

        return sb.toString();
    }

    private String escapeCharToRtf(char c) {
        switch (c) {
            case '}':
                return "\\}";
            case '{':
                return "\\{";
            case '\n':
                return "\\par" + eol;
            case '\r':
                return "\\par" + eol;
            case '\t':
                return "\\tab";
            case ' ':
                return " ";
            default:
                return String.valueOf(c);
        }
    }

    private String escapeUtfCharsToRtf(final String oldcontent) {
        StringBuilder ret = new StringBuilder();

        for (char ch : oldcontent.toCharArray()) {
            if (ch == '\\') {
                ret.append("\\\\");
            } else if (ch < 0x7f) {
                ret.append(ch);
            } else {
                // convert UTF character to its ASCII representation "\ u<decimal code>?"
                ret.append("\\u");
                ret.append((int) ch);
                ret.append("?");
            }
        }
        return ret.toString();
    }

    private String buildRtfFromTokens(Color defColor, List<TokenWithStyle> tokensWithStyle) {
        ColorTable colorTable = createColorTable(defColor, tokensWithStyle);
        String colorTableRtf = colorTableToRtf(colorTable);
        String fontTableRtf = fontTableToRtf();
        String content = createContentRtf(tokensWithStyle, colorTable);
        return buildRtf(fontTableRtf, colorTableRtf, content);
    }

    private String createContentRtf(List<TokenWithStyle> tokensWithStyle, ColorTable colorTable) {
        final StringBuilder sb = new StringBuilder();
        for (TokenWithStyle tokenWithStyle : tokensWithStyle) {
            String tokenRtf = tokenToRtf(tokenWithStyle.getToken());

            Font font = tokenWithStyle.getFont();
            if (font != null) {
                tokenRtf = wrapTokenRtfInFontStyle(font.getStyle(), tokenRtf);
            }

            Color fg = tokenWithStyle.getFg();
            if (fg != null) {
                tokenRtf = wrapTokenRtfInColor(colorTable, fg, tokenRtf);
            }
            sb.append(tokenRtf);
        }
        return sb.toString();
    }

    private String wrapTokenRtfInFontStyle(int fs, String rtf) {
        switch (fs) {
            case Font.BOLD:
                return "{\\b " + rtf + "}";
            case Font.ITALIC:
                return "{\\i " + rtf + "}";
            case Font.BOLD + Font.ITALIC:
                return "{\\b\\i " + rtf + "}";
            default:
        }
        return rtf;
    }

    private String wrapTokenRtfInColor(ColorTable colorTable, Color fg, String tokenRtf) {
        Integer fgId = colorTable.getColorIndex(fg) + 1;
        return "{\\cf" + fgId + " " + tokenRtf + "}";
    }

    private String colorTableToRtf(ColorTable colorTable) {
        StringBuilder sb = new StringBuilder();

        for (Color color : colorTable.getColorsList()) {
            sb.append(colorToRtf(color)).append(";");
        }
        return "{\\colortbl;" + sb.toString() + "}";
    }

}
