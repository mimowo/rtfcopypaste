package org.netbeans.modules.rtfcopypaste.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;

public class DefaultRtfConverter extends RtfConverter {

    private static final Logger LOGGER = Logger.getLogger(DefaultRtfConverter.class.getName());

    @Override
    protected List<TokenWithStyle> createTokensWithStyle(JEditorPane pane,
            FontColorSettings fcs) {
        final TokenSequence<?> ts = TokenHierarchy.get(pane.getDocument()).
                tokenSequence();
        List<TokenWithStyle> tokensWithStyle = new ArrayList<TokenWithStyle>();
        ts.move(pane.getSelectionStart());

        while (ts.moveNext() && ts.offset() < pane.getSelectionEnd()) {
            Token<?> token = ts.token();

            TokenWithStyle tokenWithStyle = new TokenWithStyle(token.text().toString());
            String ID = token.id().primaryCategory();
            AttributeSet attrs = fcs.getTokenFontColors(ID);
            updateTokenStyle(tokenWithStyle, attrs);
            tokensWithStyle.add(tokenWithStyle);
        }
        for (TokenWithStyle token : tokensWithStyle) {
            LOGGER.info("token def: " + token.getToken());
        }
        return tokensWithStyle;
    }

}
