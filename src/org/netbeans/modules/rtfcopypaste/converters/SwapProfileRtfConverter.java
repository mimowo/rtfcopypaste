package org.netbeans.modules.rtfcopypaste.converters;

import javax.swing.JEditorPane;
import org.netbeans.modules.rtfcopypaste.utils.ConverterSettings;
import org.netbeans.modules.rtfcopypaste.utils.EditorProfileManager;

public class SwapProfileRtfConverter {

    public static final String HIGHLIGHTS = "HIGHTLIGHTS";
    public static final String DEFAULT = "DEFAULT";
    private final RtfConverter rtfConverter;

    public SwapProfileRtfConverter(RtfConverter rtfConverter) {
        this.rtfConverter = rtfConverter;
    }

    public String convertContentToRtf(JEditorPane pane) {

        String currentProfile = EditorProfileManager.getDefault().getCurrentFontAndColorsProfile();
        String selectedProfile = ConverterSettings.getDefault().getSelectedProfile();

        if (currentProfile.equals(selectedProfile)) {
            return rtfConverter.convertContentToRtf(pane);
        } else {
            EditorProfileManager.getDefault().setCurrentFontAndColorProfile(selectedProfile);
            String rtftext = rtfConverter.convertContentToRtf(pane);
            EditorProfileManager.getDefault().setCurrentFontAndColorProfile(currentProfile);
            return rtftext;
        }
    }
}
