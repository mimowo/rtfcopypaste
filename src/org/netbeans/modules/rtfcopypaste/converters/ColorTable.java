package org.netbeans.modules.rtfcopypaste.converters;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ColorTable {
    private Map<Color, Integer> colMap = new HashMap<Color, Integer>();
    private List<Color> colList = new ArrayList<Color>();
    
    public ColorTable(Color defColor) {
        colMap.put(defColor, 0);
        colList.add(defColor);
    }
    
    public List<Color> getColorsList() {
        return colList;
    }
    
    public void addColor(Color color) {
        if (!colMap.containsKey(color)) {
            colMap.put(color, colMap.size());
            colList.add(color);
        }
    }
    
    public Integer getColorIndex(Color color) {
        return colMap.get(color);
    }
}
