package org.netbeans.modules.rtfcopypaste.converters;

import java.awt.Color;
import java.awt.Font;

public class TokenWithStyle {

    private String token;
    private Color fg;
    private Color bg;
    private Font font;

    public TokenWithStyle(String token) {
        this.token = token;
    }
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Color getFg() {
        return fg;
    }

    public void setFg(Color fg) {
        this.fg = fg;
    }

    public Color getBg() {
        return bg;
    }

    public void setBg(Color bg) {
        this.bg = bg;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

}
