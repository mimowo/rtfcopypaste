package org.netbeans.modules.rtfcopypaste.action;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import org.netbeans.modules.rtfcopypaste.RTFTransferable;
import org.netbeans.modules.rtfcopypaste.converters.DefaultRtfConverter;
import org.netbeans.modules.rtfcopypaste.converters.HighlightsRtfConverter;
import org.netbeans.modules.rtfcopypaste.converters.RtfConverter;
import org.netbeans.modules.rtfcopypaste.converters.SwapProfileRtfConverter;
import static org.netbeans.modules.rtfcopypaste.converters.SwapProfileRtfConverter.HIGHLIGHTS;
import org.netbeans.modules.rtfcopypaste.utils.ConverterSettings;
import org.openide.cookies.EditorCookie;
import org.openide.util.Utilities;

public class RTFCopyPasteAction extends AbstractAction {

    private static final Logger LOGGER = Logger.getLogger(RTFCopyPasteAction.class.getName());

    private static final RTFCopyPasteAction INSTANCE = new RTFCopyPasteAction();

    public static RTFCopyPasteAction getDefault() {
        return INSTANCE;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        action();
    }

    private void action() {
        final EditorCookie editorCookie
                = Utilities.actionsGlobalContext().lookup(EditorCookie.class);
        if (editorCookie == null) {
            return;
        }
        for (final JEditorPane pane : editorCookie.getOpenedPanes()) {
            if (pane != null && pane.isShowing()) {
                String rtf = convertContentToRtf(pane);
                putRtfToClipboard(rtf);
                return;
            }
        }
    }

    private String convertContentToRtf(final JEditorPane pane) {
        RtfConverter rtfConverter = selectRtfConverter();
        SwapProfileRtfConverter converter = new SwapProfileRtfConverter(rtfConverter);
        return converter.convertContentToRtf(pane);
    }

    private void putRtfToClipboard(String rtf) {
        ByteArrayInputStream bais = new ByteArrayInputStream(rtf.getBytes());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new RTFTransferable(bais), null);
    }

    private RtfConverter selectRtfConverter() {
        LOGGER.log(Level.INFO, "selecting: {0}", ConverterSettings.getDefault().getSelectedConverter());
        if (ConverterSettings.getDefault().getSelectedConverter().equals(HIGHLIGHTS)) {
            return new HighlightsRtfConverter();
        } else {
            return new DefaultRtfConverter();
        }
    }
}
