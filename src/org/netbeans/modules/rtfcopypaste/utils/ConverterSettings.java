package org.netbeans.modules.rtfcopypaste.utils;

import org.netbeans.modules.rtfcopypaste.converters.SwapProfileRtfConverter;
import org.netbeans.modules.rtfcopypaste.options.SpinnerFontModel;

public class ConverterSettings {

    private String selectedProfile;
    private Integer selectedFontSize;
    private String selectedConverterStr;
    
    private static final ConverterSettings INSTANCE = new ConverterSettings();

    public static ConverterSettings getDefault() {
        return INSTANCE;
    }
    
    public ConverterSettings() {
        selectedProfile = DefaultEditorProfileManager.getDefault().getCurrentFontAndColorsProfile();
        selectedFontSize = SpinnerFontModel.getDefaultValue();
        selectedConverterStr = SwapProfileRtfConverter.DEFAULT;
    }
    
    public String getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(String profile) {
        selectedProfile = profile;
    }

    public Integer getSelectedFontSize() {
        return selectedFontSize;
    }

    public void setSelectedFontSize(Integer fontSize) {
        selectedFontSize = fontSize;
    }

    public String getSelectedConverter() {
        return selectedConverterStr;
    }

    public void setSelectedConverterStr(String converterDesc) {
        selectedConverterStr = converterDesc;
    }
}
